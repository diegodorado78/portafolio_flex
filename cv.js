$(document).ready(function(){
$(".boton-mas").click(function(){
    $("#span-mas").toggle("fast");
});
$('.bxslider').bxSlider({
    auto: true,
    autoControls: true,
    stopAutoOnClick: true,
    pager: true,
    slideWidth: "1200",
  });

  //TOMAR INFO JSON Y AGREGARLA AL HTML COMO APPEND
  var fecha = new Date();
  var posts =[
      {
        title:"ALCALDIA DE POPAYÁN",
        date: fecha.getFullYear(),
        content:" Asesoría y capacitación en formulación de proyectos a ciudadanos de Popayán pertenecientes a diversas lineas productivas.",
      },
      {
        title:"DNA FOCUS YOUR MIND ",
        date: fecha.getFullYear(),
        content:" Enseñanza de inglés a través de escenarios reales donde estudiantes  mejoran su nivel conversacional por medio de un proceso de inmersión. ",
      },
      {
        title:"SOMBRERON ARTE RUSTICO ",
        date: fecha.getFullYear(),
        content:" Creación de campañas de marketing digital enfocadas en la generación y conversión de leads.",
      },
      {
        title:"ONG AIESEC",
        date: fecha.getFullYear(),
        content:" Miembro de equipos en las áreas de recursos humanos, comunicaciones y delivery de experiencia para participantes internacionales.",
      },
  ];
  posts.forEach(item => {
      var post= `
      
      <h2>  ${item.title}</h2>
      <p>
      ${item.content}
      </p>
     
`;   
$(".post2").append(post);
$("#acordeon").append(post);
});
//CAMBIO DE TEMA
var theme= $("#theme");
$("#to-blue").click(function(){
    theme.attr("href","./cv.css");
});
$("#to-black").click(function(){
    theme.attr("href","./black.css");
});
$("#to-green").click(function(){
    theme.attr("href","./green.css");
});
//scroll arriba
$(".subir").click(function(e){
    e.preventDefault();
   $("html, body").animate({
       scrollTop:0
   },500);
   return false;
});
// login falso
$("#login").submit(function(){
    var form_name = $("#name").val();
    localStorage.setItem("form_name",form_name);
});
var form_name = localStorage.getItem("form_name");

if(form_name != null && form_name !="undefined"){
   about_h3= $("#about h3");
    about_h3.html("Bienvenido, "+ form_name+"<br>");
    about_h3.append("<a href='#' id ='logout'> Cerrar Sesión </a>");

    $("#login").hide();

    $("#logout").click(function(){
        localStorage.clear();
        location.reload();
    });
};
// compruebo si en el nombre de la pag esta 'experiencia' entonces ejecuta el trozo de cod
      if(window.location.href.indexOf('experiencia')> -1){
          $('#acordeon').accordion();
      };   
      //reloj
      setInterval(function(){
        var reloj= moment().format('MMMM Do YYYY, h:mm:ss a');
        $("#reloj").html(reloj);
      },1000);// que se ejecute cada segundo el intervalo
      
      //pagina de contacto
      if(window.location.href.indexOf('contacto')> -1){
      $("form input[name='date']").datepicker({
        dateFormat: 'dd/mm/yy'
      });
        $.validate({
        lang: 'es',
        errorMessagePosition: 'top',
        scrollTopOnError: true,
      });
    };
});